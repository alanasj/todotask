# README #

### What is this repository for? ###

* TodoTaskApplication, with java springboot backend and angular front (task 1 and 2)

### How do I get set up? ###

* Checkout repository, and import pom file
* Run backend, it will run on 8080 localhost port
* Run terminal in src/main/resources/static/angularSide , requires npm manager installed
* run (npm install) and after installation finishes  ( ng serve --open) it will run angular on 4200 localhost port
