package alano.worktask.todolist.entities;

import javax.persistence.*;

@Entity
@Table(name = "TASK")
public class Task {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "TASK_DESCRIPTION")
    private String taskDescription;

    @Column(name = "ARCHIVE")
    private boolean archive;

    public Task() {
    }

    public Task(String taskDescription, boolean archive) {
        this.taskDescription = taskDescription;
        this.archive = archive;
    }

    public boolean getArchive() {
        return archive;
    }

    public void setArchive(boolean archive) {
        this.archive = archive;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

}