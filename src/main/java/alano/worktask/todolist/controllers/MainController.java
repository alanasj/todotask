package alano.worktask.todolist.controllers;

import alano.worktask.todolist.entities.Task;
import alano.worktask.todolist.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
public class MainController {

    private final TaskRepository taskRepository;

    @Autowired
    public MainController(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @GetMapping("/tasks")
    public List<Task> getTasks() {
        return taskRepository.findAllByArchiveFalse();
    }

    @PostMapping("/task")
    public Task createTask(@RequestBody Task task) {
        return taskRepository.save(task);
    }

    @PutMapping("/task/{id}")
    public Task archiveTask(@PathVariable Long id) {
        Task updateTask = taskRepository.getOne(id);
        updateTask.setArchive(true);
        return taskRepository.save(updateTask);
    }

    @GetMapping("/archive")
    public List<Task> getArchivedTask() {
        return taskRepository.findAllByArchiveTrue();
    }

}
