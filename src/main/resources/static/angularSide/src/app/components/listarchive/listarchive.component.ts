import { Component, OnInit } from '@angular/core';
import {TaskService} from "../../shared_service/task.service";

@Component({
  selector: 'app-list-archive',
  templateUrl: './listarchive.component.html',
  styleUrls: ['./listarchive.component.css']
})
export class ListArchiveComponent implements OnInit {
  tasks: any;

  constructor(private taskService: TaskService) {
  }

  ngOnInit() {
    this.getArchivedTasks()
  }

  private getArchivedTasks() {
    this.taskService.getArchivedTasks().subscribe((tasks) => {
      console.log(tasks);
      this.tasks = tasks;
    },(error)=>{
      console.log(error);
    })
  }

}
