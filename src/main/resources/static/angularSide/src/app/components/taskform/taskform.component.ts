import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {TaskService} from "../../shared_service/task.service";
import {Task} from '../../task';

@Component({
  selector: 'app-task-form',
  templateUrl: './taskform.component.html',
  styleUrls: ['./taskform.component.css']
})
export class TaskFormComponent implements OnInit {
  private task: Task;

  constructor(private taskService: TaskService, private router: Router) {
  }

  ngOnInit() {
    this.task = this.taskService.getter();
  }

  submitForm() {
    this.taskService.createTask(this.task).subscribe((task) => {
      this.router.navigate(['/'])
    }, (error) => {
      console.log(error)
    });
  }

}
