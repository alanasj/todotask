import { Component, OnInit } from '@angular/core';
import {TaskService} from "../../shared_service/task.service";
import {Router} from "@angular/router";
import {Task} from "../../task";

@Component({
  selector: 'app-listtask',
  templateUrl: './list-task.component.html',
  styleUrls: ['./list-task.component.css']
})

export class ListTaskComponent implements OnInit {
  tasks: any;

  constructor(private taskService: TaskService, private router: Router) {
  }

  ngOnInit() {
    this.getTasks();
  }

  archiveTask(task) {
    this.taskService.archiveTask(task.id).subscribe((data) => {
      this.tasks.splice(this.tasks.indexOf(task), 1)
    }, error => {
      console.log(error)
    })
  }

  createNewTask() {
    let task = new Task();
    this.taskService.setter(task);
    this.router.navigate(['/create_task'])
  }

  private getTasks() {
    this.taskService.getTasks().subscribe((tasks) => {
      console.log(tasks);
      this.tasks = tasks;
    }, (error) => {
      console.log(error);
    })
  }

}
