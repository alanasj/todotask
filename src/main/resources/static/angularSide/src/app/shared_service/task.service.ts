import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {catchError} from "rxjs/operators";
import {Observable} from "rxjs/internal/Observable";
import {throwError} from "rxjs/internal/observable/throwError";
import {Task} from '../task';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class TaskService {
  private baseUrl: string = 'http://localhost:8080/api';
  private task: Task;

  constructor(private http: HttpClient) {
  }

  getTasks() {
    return this.http.get(this.baseUrl + '/tasks').pipe(catchError(this.handleError))
  }

  createTask(task: Task): Observable<Task> {
    return this.http.post<Task>(this.baseUrl + '/task/', task, httpOptions).pipe(catchError(this.handleError))
  }

  archiveTask(id: Number): Observable<Task> {
    return this.http.put<Task>(this.baseUrl + '/task/' + id, httpOptions).pipe(catchError(this.handleError))
  }

  getArchivedTasks() {
    return this.http.get(this.baseUrl + '/archive').pipe(catchError(this.handleError))
  }

  setter(task: Task) {
    this.task = task;
  }

  getter() {
    return this.task;
  }

  private handleError(error: HttpErrorResponse) {
    console.error("An error occured:", error.message);
    return throwError(
      "Please try again, something went wrong"
    );
  };

}
