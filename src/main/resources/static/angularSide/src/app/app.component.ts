import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angularSide';
  constructor(private router:Router){
    if(this.openArchive){
      this.router.navigate(['/archive'])
    }
  }

  openTasks(){
    this.router.navigate(['/'])
  }

  openArchive(){
    this.router.navigate(['/archive'])
  }

}
