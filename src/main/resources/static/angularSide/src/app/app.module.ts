import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ListTaskComponent} from './components/listtask/list-task.component';
import {ListArchiveComponent} from './components/listarchive/listarchive.component';
import {TaskFormComponent} from './components/taskform/taskform.component';
import {RouterModule, Routes} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {TaskService} from "./shared_service/task.service";

const appRoutes: Routes = [
  {path: '', component: ListTaskComponent},
  {path: 'create_task', component: TaskFormComponent},
  {path: 'archive', component: ListArchiveComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    ListTaskComponent,
    TaskFormComponent,
    ListArchiveComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [TaskService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
