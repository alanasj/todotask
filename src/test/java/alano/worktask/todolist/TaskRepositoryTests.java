package alano.worktask.todolist;


import alano.worktask.todolist.entities.Task;
import alano.worktask.todolist.repository.TaskRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TaskRepositoryTests {

    @Autowired
    private TaskRepository taskRepository;

    private List<Task> archivedList;
    private List<Task> taskList;

    @Test
    public void saveAndFind(){
        Task task = new Task("Test",true);
        Task task2 = new Task("Test2",false);

        taskRepository.save(task);
        taskRepository.save(task2);

        archivedList = new ArrayList<>();
        taskList = new ArrayList<>();

        archivedList = taskRepository.findAllByArchiveTrue();
        taskList = taskRepository.findAllByArchiveFalse();

        assertEquals(1,archivedList.size());
        assertEquals(1,archivedList.size());

    }

    @Test
    public void updateAndFind(){
        Task task = new Task("Test",true);
        Task task2 = new Task("Test2",false);

        taskRepository.save(task);
        taskRepository.save(task2);

        archivedList = new ArrayList<>();
        taskList = new ArrayList<>();

        task.setArchive(false);
        taskRepository.save(task);

        archivedList = taskRepository.findAllByArchiveTrue();
        taskList = taskRepository.findAllByArchiveFalse();

        assertEquals(0,archivedList.size());
        assertEquals(2,taskList.size());

        task2.setArchive(true);
        task.setArchive(true);
        taskRepository.save(task);
        taskRepository.save(task2);

        archivedList = taskRepository.findAllByArchiveTrue();
        taskList = taskRepository.findAllByArchiveFalse();

        assertEquals(2,archivedList.size());
        assertEquals(0,taskList.size());

    }

}
