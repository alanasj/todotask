package alano.worktask.todolist;

import alano.worktask.todolist.controllers.MainController;
import alano.worktask.todolist.entities.Task;
import alano.worktask.todolist.repository.TaskRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(MainController.class)
public class ControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TaskRepository taskRepository;

    @Test
    public void getTasks() throws Exception {
        Task task = new Task("Test", true);
        Task task2 = new Task("Test2", false);
        Task task3 = new Task("Test3", true);
        Task task4 = new Task("Test4", false);

        taskRepository.save(task);
        taskRepository.save(task2);
        taskRepository.save(task3);
        taskRepository.save(task4);

        List<Task> tasks = Arrays.asList(task2, task4);

        given(taskRepository.findAllByArchiveFalse()).willReturn(tasks);

        mvc.perform(get("/api/tasks")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void getArchivedTasks() throws Exception {
        Task task = new Task("Test", true);
        Task task2 = new Task("Test2", false);
        Task task3 = new Task("Test3", true);
        Task task4 = new Task("Test4", false);

        taskRepository.save(task);
        taskRepository.save(task2);
        taskRepository.save(task3);
        taskRepository.save(task4);

        List<Task> tasks = Arrays.asList(task, task3);

        given(taskRepository.findAllByArchiveTrue()).willReturn(tasks);

        mvc.perform(get("/api/archive")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

}
